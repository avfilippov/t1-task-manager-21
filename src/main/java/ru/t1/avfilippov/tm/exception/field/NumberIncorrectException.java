package ru.t1.avfilippov.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(final String value, Throwable clause) {
        super("Error! This value\"" + value + "\"is incorrect...");
    }

}
