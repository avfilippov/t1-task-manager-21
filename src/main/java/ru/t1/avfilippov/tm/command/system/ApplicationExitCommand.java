package ru.t1.avfilippov.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "close application";
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public void execute() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}
