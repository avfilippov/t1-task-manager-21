package ru.t1.avfilippov.tm.command.user;

import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String NAME = "update-user-profile";

    private final String DESCRIPTION = "update profile of current user";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

}
